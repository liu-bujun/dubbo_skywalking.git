package com.liubujun.redis_springboot.controller;

import com.liubujun.redis_springboot.entity.User;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author: liubujun
 * @Date: 2022/2/2 16:17
 */

@RestController
@RequestMapping("/redisTest")
public class TestController {

    @Resource
    private RedisTemplate redisTemplate;

    @GetMapping("/redis")
    public String testRedis(){
        redisTemplate.opsForValue().set("test","abc");
        String string = (String)redisTemplate.opsForValue().get("test");
        return string;

    }


    @GetMapping("/cache/{id}")
    @Cacheable(cacheNames = "User",unless = "#id >1 ") //这里表示id值大于1时不会进行缓存
    public User findUserByIds(@PathVariable("id") Integer id){
        User user = new User();
        return user;
    }

    @GetMapping("/cache/{id}")
    @Cacheable(cacheNames = "User",key = "#root.methodName+'[' + #id + ']'") //这时如果传入的id为500，那么key=findUserById[500]
    public User findUserById(@PathVariable("id") Integer id){
        User user = new User();
        return user;
    }
}
