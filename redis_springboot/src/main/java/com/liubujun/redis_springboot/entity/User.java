package com.liubujun.redis_springboot.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: liubujun
 * @Date: 2022/2/5 15:44
 */

@Data
public class User implements Serializable {

    private Integer id;
    private String name;

}
