package com.liubujun;

import org.junit.Test;
import redis.clients.jedis.Jedis;

import java.util.List;
import java.util.Set;

/**
 * @Author: liubujun
 * @Date: 2022/2/1 17:36
 */


public class JedisDemo {

    public static void main(String[] args) {
        //创建jedis对象
        Jedis jedis = new Jedis("47.118.53.22",6379);
        //查看redis能否ping通
        String ping = jedis.ping();
        System.out.println(ping);
    }

    @Test
    public void demo1(){
        //创建jedis对象
        Jedis jedis = new Jedis("47.118.53.22",6379);
        jedis.mset("k1","v1","k2","v2");
        List<String> mget = jedis.mget("k1", "k2");
        System.out.println(mget);

        Set<String> keys = jedis.keys("*");
        for (String key : keys) {
            System.out.println(key);
        }

    }

    @Test
    public void demo2(){
        //创建jedis对象
        Jedis jedis = new Jedis("47.118.53.22",6379);
        jedis.lpush("key1","lucy","marry","jack");
        List<String> key1 = jedis.lrange("key1", 0, -1);
        System.out.println(key1);
    }

    @Test
    public void demo3(){
        //创建jedis对象
        Jedis jedis = new Jedis("47.118.53.22",6379);
        jedis.sadd("names","lucy","marry");
        Set<String> names = jedis.smembers("names");
        System.out.println(names);
    }

    @Test
    public void demo4(){
        //创建jedis对象
        Jedis jedis = new Jedis("47.118.53.22",6379);
        jedis.hset("users","age","20");
        String hget = jedis.hget("users", "age");
        System.out.println(hget);
    }

    @Test
    public void demo5(){
        //创建jedis对象
        Jedis jedis = new Jedis("47.118.53.22",6379);
        jedis.zadd("china",100d,"shanghai");
        Set<String> china = jedis.zrange("china", 0, -1);
        System.out.println(china);
    }
}
