package com.example.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author: liubujun
 * @Date: 2021/11/21 13:22
 */

@RestController
public class TestController {

    @GetMapping("/test/test")
    public String test(String username, HttpServletRequest request){
        //认证成功之后返回session
        request.getSession().setAttribute("username",username);
    return "ok";
    }
}
