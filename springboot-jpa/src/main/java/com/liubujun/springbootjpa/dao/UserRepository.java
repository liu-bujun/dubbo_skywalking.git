package com.liubujun.springbootjpa.dao;

import com.liubujun.springbootjpa.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @Author: liubujun
 * @Date: 2021/11/20 16:29
 */


public interface UserRepository extends JpaRepository<User,Integer>, JpaSpecificationExecutor<User> {
}
