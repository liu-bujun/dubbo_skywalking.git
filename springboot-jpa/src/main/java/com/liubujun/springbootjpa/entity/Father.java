package com.liubujun.springbootjpa.entity;

import lombok.Data;

/**
 * @Author: liubujun
 * @Date: 2021/11/26 17:07
 */

@Data
public class Father {

    private String face;
    private String hight;
    private String life;

}
