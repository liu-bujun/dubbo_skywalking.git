package com.liubujun.springbootjpa.entity;

import lombok.Data;

/**
 * @Author: liubujun
 * @Date: 2021/11/26 17:08
 */

@Data
public class Life {

    private String status;
}
