package com.liubujun.springbootjpa.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @Author: liubujun
 * @Date: 2021/10/21 17:38
 */
@Entity
@Table(name = "t_user")
@Data
public class User implements Serializable {
    @Id
    private Integer id;
    private String name;
    private String address;
    private Integer age;
    private Date createTime;

}
