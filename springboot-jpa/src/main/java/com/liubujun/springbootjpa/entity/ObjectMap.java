package com.liubujun.springbootjpa.entity;

import lombok.Builder;
import lombok.Data;

/**
 * @Author: liubujun
 * @Date: 2021/11/24 13:51
 */

@Data
@Builder
public class ObjectMap {
    private String key;
    private String value;
}
