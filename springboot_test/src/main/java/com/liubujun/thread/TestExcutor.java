package com.liubujun.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Author: liubujun
 * @Date: 2021/11/8 10:55
 */


public class TestExcutor {


    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        while (true) {
            executorService.execute(new Task());
        }
    }

    static class Task implements Runnable{

        @Override
        public void run() {
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
