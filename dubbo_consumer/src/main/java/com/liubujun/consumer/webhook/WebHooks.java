package com.liubujun.consumer.webhook;

import com.liubujun.consumer.controller.AlarmController;
import com.liubujun.consumer.pojo.AlarmMessage;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: liubujun
 * @Date: 2021/11/6 15:23
 */

@RestController
public class WebHooks {

    private List<AlarmMessage> lastList = new ArrayList<AlarmMessage>();

    /**
     * 从webhook接收告警信息
     * @param alarmMessageList
     */
    @PostMapping("/webhook")
    public void webhook(@RequestBody List<AlarmMessage> alarmMessageList){
        lastList = alarmMessageList;
    }

    /**
     * 用来验证RocketBot上的告警信息是否和接收到的是否一致
     * @return
     */
    @GetMapping("/show")
    public List<AlarmMessage> show(){
        return lastList;
    }

}
