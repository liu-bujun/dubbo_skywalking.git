package com.liubujun.consumer.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: liubujun
 * @Date: 2021/11/6 15:20
 */

@RestController
public class AlarmController {

    /**
     * 模拟超时报警
     * @return
     */
    @RequestMapping("/timeout")
    public String timeout(){
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "timeout";
    }
}
