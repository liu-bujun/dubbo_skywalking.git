package com.liubujun.consumer.controller;


import com.liubujun.entity.User;
import com.liubujun.provider.service.OrderService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.config.annotation.DubboService;
import org.apache.skywalking.apm.toolkit.trace.ActiveSpan;
import org.apache.skywalking.apm.toolkit.trace.TraceContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author: liubujun
 * @Date: 2021/10/22 16:34
 */

@RestController
public class ConsumerController {


    @DubboReference
    private OrderService orderService;



    //测试消费方调用服务方
    @RequestMapping("/getUser")
    public List<User> getUser(){
        return orderService.getUser();
    }



    //获取追踪Id，并可以在rocketBot中查询
    @RequestMapping("/getTraceId")
    public String getTraceId(){
        //使当前链路报错，并提示报错信息
        ActiveSpan.error(new RuntimeException("Test-Error-Throwable"));
        //打印当前info信息
        ActiveSpan.info("Test-Error-Throwable");
        //打印debug信息
        ActiveSpan.debug("Test-debug-Throwable");
        //获取tranceId
        return TraceContext.traceId();
    }

    @RequestMapping("/getError")
    public void getError(){
        int i = 1/0;
    }


    @RequestMapping("/include")
    public String getInclude(){
        return "没有忽略";
    }

    @RequestMapping("/ignore")
    public String getIgnore(){
        return " 忽略";
    }
}
