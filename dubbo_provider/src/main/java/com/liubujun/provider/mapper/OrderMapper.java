package com.liubujun.provider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.liubujun.entity.User;
import org.springframework.stereotype.Repository;

/**
 * @Author: liubujun
 * @Date: 2021/10/27 16:42
 */

@Repository
public interface OrderMapper extends BaseMapper<User> {

}
