package com.liubujun.provider.serviceImpl;



import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.liubujun.entity.User;
import com.liubujun.provider.mapper.OrderMapper;
import com.liubujun.provider.service.OrderService;
import org.apache.dubbo.config.annotation.DubboService;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author: liubujun
 * @Date: 2021/10/21 17:47
 */


@DubboService
public class OrderServiceImpl implements OrderService {

    @Resource
    private OrderMapper orderMapper;

    @Override
    public List<User> getUser() {
        QueryWrapper<User> queryWrapper = new QueryWrapper();
        queryWrapper.eq("name","王美丽");
        List<User> users = orderMapper.selectList(queryWrapper);
        return users;
    }
}
