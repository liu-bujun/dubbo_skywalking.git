package com.liubujun.provider.service;


import com.liubujun.entity.User;

import java.util.List;

/**
 * @Author: liubujun
 * @Date: 2021/10/21 17:46
 */


public interface OrderService {

    List<User> getUser();
}
