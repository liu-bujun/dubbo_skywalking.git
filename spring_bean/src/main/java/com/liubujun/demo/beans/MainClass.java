package com.liubujun.demo.beans;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @Author: liubujun
 * @Date: 2022/2/10 13:55
 */


public class MainClass {



    public static void main(String[] args) {
        AnnotationConfigApplicationContext ioc = new AnnotationConfigApplicationContext(MainConfig.class);

        //获取bean getBean获取
        InstanceB instanceB = (InstanceB) ioc.getBean("instanceB");
    }
}
