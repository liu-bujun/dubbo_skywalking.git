package com.liubujun.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * @Author: liubujun
 * @Date: 2021/11/24 16:33
 */


public class Hospital {
    /**
     * 医院编码
     */
    @Excel(name = "势成医院编码")
    private String code;

    /**
     * 医院名称
     */
    @Excel(name = "势成医院名称")
    private String name;


    /**
     * 美年编码
     */
    @Excel(name = "美年编码")
    private String meiNianCode;

    /**
     * 美年医院名称
     */
    @Excel(name = "美年医院名称")
    private String meiNianName;

    public Hospital(String code, String name, String meiNianCode, String meiNianName) {
        this.code = code;
        this.name = name;
        this.meiNianCode = meiNianCode;
        this.meiNianName = meiNianName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMeiNianCode() {
        return meiNianCode;
    }

    public void setMeiNianCode(String meiNianCode) {
        this.meiNianCode = meiNianCode;
    }

    public String getMeiNianName() {
        return meiNianName;
    }

    public void setMeiNianName(String meiNianName) {
        this.meiNianName = meiNianName;
    }



    @Override
    public String toString() {
        return "Hospital{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", meiNianCode='" + meiNianCode + '\'' +
                ", meiNianName='" + meiNianName + '\'' +
                '}';
    }
}
