package com.liubujun.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;

import java.util.List;

/**
 * @Author: liubujun
 * @Date: 2021/12/14 10:28
 */



public class HB {


    private Integer id;

    private List<A> aa;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<A> getAa() {
        return aa;
    }

    public void setAa(List<A> aa) {
        this.aa = aa;
    }

    @Override
    public String toString() {
        return "HB{" +
                "id=" + id +
                ", aa=" + aa +
                '}';
    }
}
