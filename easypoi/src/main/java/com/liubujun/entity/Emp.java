package com.liubujun.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;

import java.util.Date;

/**
 * @Author: liubujun
 * @Date: 2021/11/7 18:15
 */
@ExcelTarget("emps")
public class Emp {

    @Excel(name = "编号")
    private Integer id;
    @Excel(name = "姓名")
    private String name;
    @Excel(name = "年龄",suffix = "*") //在列的后面加上"*"
    private Integer age;
    @Excel(name = "生日",width = 35.0,format = "yyyy-MM-dd")
    private Date bir;
    @Excel(name = "状态",replace = {"未删除_1","删除_0"}) //用"未删除替换1"
    private String status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Date getBir() {
        return bir;
    }

    public void setBir(Date bir) {
        this.bir = bir;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Emp{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", bir=" + bir +
                ", status='" + status + '\'' +
                '}';
    }
}
