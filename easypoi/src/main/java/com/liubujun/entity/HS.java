package com.liubujun.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * @Author: liubujun
 * @Date: 2021/12/6 16:47
 */


public class HS {

    @Excel(name = "id")
    private Integer id;

    @Excel(name = "open_order_supplier")
    private String code;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
