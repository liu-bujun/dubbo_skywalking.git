package com.liubujun.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * @Author: liubujun
 * @Date: 2021/12/6 16:47
 */


public class HC {

    @Excel(name = "渠道编码")
    private String id;

    @Excel(name = "租户id")
    private String code;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
