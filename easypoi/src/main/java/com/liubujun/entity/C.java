package com.liubujun.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * @Author: liubujun
 * @Date: 2022/1/11 18:34
 */


public class C {

    @Excel(name = "医院id")
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "C{" +
                "id=" + id +
                '}';
    }
}
