package com.liubujun.proxy.zhongjianshang;

import com.liubujun.proxy.factory.UsbFactory;
import com.liubujun.proxy.service.UsbSell;

/**
 * @Author: liubujun
 * @Date: 2021/12/8 18:06
 */

//代理类-----中间商
public class TaoBao implements UsbSell {

    //代理的是金士顿 定义目标厂家类
    private UsbFactory usbFactory =  new UsbFactory();

    @Override
    public float sell(int amount) {
        //调用目标方法
        float price = usbFactory.sell(amount);

        //增强功能一
        price =  (price + 15)*amount;
        //增强功能二
        System.out.println("您购买了我们的商品，给您返了一张优惠券了");
        return price;
    }
}
