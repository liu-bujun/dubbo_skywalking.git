package com.liubujun.proxy.factory;

import com.liubujun.proxy.service.UsbSell;

/**
 * @Author: liubujun
 * @Date: 2021/12/8 17:50
 */

//目标类 厂家只向中间商供货，不会想个体小商户供货
public class UsbFactory implements UsbSell {
    @Override
    public float sell(int amount) {
        //定义一个U盘的价格是65元
        float price = 65.0f;
        return amount*price;
    }
}
