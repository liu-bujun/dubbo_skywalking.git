package com.liubujun.proxy.service;

/**
 * @Author: liubujun
 * @Date: 2021/12/8 17:51
 */


public interface UsbSell {

    /**
     * 定义一个方法 厂家，商家都要用完成的功能
     * @param amount 购买的数量
     * @return
     */
    float sell(int amount);
}
