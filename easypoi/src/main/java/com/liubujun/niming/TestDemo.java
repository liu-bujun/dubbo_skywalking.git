package com.liubujun.niming;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: liubujun
 * @Date: 2022/2/14 10:54
 */


public class TestDemo {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 288; i++) {
            list.add(i);
        }

        System.out.println("list长度"+list.size());
        List<Integer> newList = new ArrayList<>();
        int toIndex = 100;
        // 装填100条数据
        for (int i = 0; i < list.size(); i += 100) {
            if (i + 100 > list.size()) {
                // 注意下标问题
                toIndex = list.size() - i;
            }
            newList = list.subList(i, i + toIndex);

            System.out.println(newList);
        }

    }
}
