package com.liubujun.niming;

/**
 * @Author: liubujun
 * @Date: 2022/2/14 9:57
 */


public class Demo1 {

    public static void main(String[] args) {
        function(
                new Animal() {
                    @Override
                    public void eat() {
                        System.out.println("狼行千里吃肉,狗行千里吃翔");
                    }
                }
        );
    }

    public static void function(Animal animal){
    animal.eat();
    }
}
