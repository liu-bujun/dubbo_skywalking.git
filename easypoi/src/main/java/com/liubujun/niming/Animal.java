package com.liubujun.niming;

/**
 * @Author: liubujun
 * @Date: 2022/2/14 9:58
 */


public abstract class Animal {
    public abstract void eat();
}
