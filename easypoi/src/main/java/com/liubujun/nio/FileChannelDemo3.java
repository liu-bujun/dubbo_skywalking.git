package com.liubujun.nio;

import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * @Author: liubujun
 * @Date: 2022/2/12 20:24
 */


public class FileChannelDemo3 {

    public static void main(String[] args) throws Exception {
        //创建两个FileChannel
        RandomAccessFile aFile = new RandomAccessFile("C:\\Users\\LiuBuJun\\Desktop\\liubujun\\00.txt","rw");
        FileChannel fromChannel1 = aFile.getChannel();

        RandomAccessFile bFile = new RandomAccessFile("C:\\Users\\LiuBuJun\\Desktop\\liubujun\\02.txt","rw");
        FileChannel toChannel1 = bFile.getChannel();

        //将fromChannel通道中的数据传输到toChannel
        long position = 0;
        long size = fromChannel1.size();
        fromChannel1.transferTo(0,size,toChannel1);

        aFile.close();
        bFile.close();
        System.out.println("over");
    }
}
